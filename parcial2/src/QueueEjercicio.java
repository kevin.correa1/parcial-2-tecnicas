import javax.swing.*;
import java.util.LinkedList;
import java.util.Queue;

public class QueueEjercicio {
    public static void main(String[] args) {
        Queue<Integer> porcentajes = new LinkedList<>();
        Queue<Double> notas = new LinkedList<>();
        Queue<Double> noticas = new LinkedList<>();

        JOptionPane.showMessageDialog(null, "Bienvenido a tu calculadora de notas" );
        int n = Integer.parseInt(JOptionPane.showInputDialog("por favor ingrese la cantidad de notas"));

        for(int i = 0; i < n; i++){
            int j = i + 1;
            int m = Integer.parseInt(JOptionPane.showInputDialog("por favor ingrese el porcentaje de la nota #" + j));
            porcentajes.add(m);
        }

        for(int i = 0; i < n; i++){
            int j = i + 1;
            double k = Double.parseDouble(JOptionPane.showInputDialog("ingrese la nota #" + j));
            notas.add(k);
        }

        for(int i = 0; i < n; i++){
            double u = porcentajes.remove();
            double v = notas.remove();
            double notica = (u/100)* v;
            noticas.add(notica);
        }

        double p = 0;
        for(int i = 0; i < n; i++){
            p = p + noticas.remove();
        }
        JOptionPane.showMessageDialog(null,"su nota final es:" + p);
    }
}
